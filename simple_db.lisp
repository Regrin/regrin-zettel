(defun pyth-to-card (card)
  (let ((number (getf card :number))
        (name (remove #\Space (getf card :name))))
    (format nil "~A~A~A.rztl" *pyth-to-zettelkasten* number name)))


(defun line-search-by-car (assoc-list key) ;Получает на вход ассоциативный список (такой как список тегов или ссылок) и ключ, если в нем есть подсписок начинающийся с ключа, возвращает его индекс, если нет, возвращаеи nil
  (defun iter (assc counter)
    (cond ((null assc) nil)
	  ((equal key (caar assc)) counter)
	  (t (iter (cdr assc) (1+ counter)))))
  (iter assoc-list 0))
  
(defun update-tags (card)
  (let ((tag-list (getf card :tags))
	(card-number (getf card :number))
	(tag-index 0))
    (dolist (tag tag-list)
      (setf tag-index (line-search-by-car *global-tag-list* tag))
      (if (null tag-index)
	  (setf *global-tag-list*
		(push (list tag card-number) *global-tag-list*))
	  (setf (nth tag-index *global-tag-list*)
		(append (nth tag-index *global-tag-list*)
			(list card-number)))))))

(defun update-links (card)
  (let ((links-list (getf card :links))
	(tail nil))
    (dolist (link links-list)
      (setf tail (append tail (cdr link))))
		     
    (setf *global-links-list* (append *global-links-list* (list (cons (getf card :name)
	                                                        tail))))))

(defun add-card-to-zettelkasten (card)
  (setf (getf card :number)
	(length *global-links-list*))

  (with-open-file (card-file (pyth-to-card card) :direction :output)
    (with-standard-io-syntax
      (print card card-file)))

  (update-tags card)

  (update-links card)

  (with-open-file (tags-file (format nil "~A~A" *pyth-to-zettelkasten* "global-tag-file")
			     :direction :output
			     :if-exists :supersede)
    (with-standard-io-syntax
      (print *global-tag-list* tags-file)))   

  (with-open-file (links-file (format nil "~A~A" *pyth-to-zettelkasten* "global-links-file")
			     :direction :output
			     :if-exists :supersede)
    (with-standard-io-syntax
      (print *global-links-list* links-file))))

(defun take-card-by-number (number)
  (if (or (null *global-links-list*) (> number (length *global-links-list*)))
      nil ;Не безопасно возвращать ложь, если карточки нет. Надо будет исправить.
      (let ((pyth (format nil
			  "~A~A~A~A"
			  *pyth-to-zettelkasten*
			  number
			  (remove #\Space (car (nth number *global-links-list*)))
			  ".rztl")))
	(with-open-file (card pyth)
	  (with-standard-io-syntax
	    (read card))))))
