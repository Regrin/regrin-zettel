#+TITLE: Интерфейс для картотеки
#+LANGUAGE: ru

* Интерфейс
  Основные возможности, которые должен предоставлять интерфейс:
  - Вывод катрочек
  - Переход по ссылкам
  - Добаление карточек
  - Добавление ссылок и тегов
  - Удаление ссылок и тегов
  Это базовые возможности. Кроме них понадобятся:
  - Удаление карточек
  - Редактирование карточки
  - Поиск по тегам
  - Показ того, какие карточки ссылаются на данную

** Командный интерпретатор
   При работе программы необходимо считывать команды пользователя. Есть два
   подхода: все аргументы в одну строку, либо каждый в своей. Второй подход
   проще, так-как не нужно разделять строку по частям. Для начала будем
   реализовывать второй подход.
   При работе приложения интерпретатор будет постоянно запрашивать команду, а
   получая её вызывать обработчик этой команды.

   #+begin_src lisp :tangle yes
(defun command-handler ()
  (format t ">>")
  (let ((command (read-line)))
    (cond ((string-equal command "показать")
	   (show-command-handler)
	   (command-handler))
	  ((string-equal command "добавить")
	   (add-command-handler)
	   (command-handler))
	  ((string-equal command "выйти")
	   (format t "Программа завершена"))

	  (t
	   (print "Нет такой команды")
	   (command-handler)))))

   #+end_src

** Список команд
   Здесь приводится список доступных сейчас комманд

*** Показать карточку
    Команда: показать
    Обработчик: show-command-handler
    Запрашивает номер карточки и при наличии её выводит карточку на экран
   #+begin_src lisp :tangle yes
(defun print-card (card)
  (format t "~A ~A~%" (getf card :number) (getf card :name))
  (format t "~{#~A ~} ~%" (getf card :tags))
  (format t "~A~%" (getf card :body))
  (dolist (link (getf card :links))
    (format t "~A - ~A~%" (first link) (second link))))

(defun show-command-handler () ;Написана не безопасно, обработка ошибок основанна на том, что функция
  (format t "Введите номер карточки") ;take-card-by-number возвращает nil если карточки с таким номером нет
  (let ((number (read)) ; Переписать ОБЕ!
	(card nil))
    (cond ((numberp number)
	   (setf card (take-card-by-number number))
	   (if (null card)
	       (format t "Карточка с таким номером отсутствует")
	       (print-card card)))
	  (t
	   (format t "Вы ввели не номер")))))
  #+end_src

*** Добавить карточку
    Команда: добавить
    Обработчик: add-command-handler
    Запрашивает название
    #+begin_src lisp :tangle yes
(defun add-command-handler ()
  (defun tag-reader (tag-list)
    (format t "Введите тег: ~%")
    (setf teg-list (push tag-list (read-line)))
    (format t "Ещё тег? (д/н) ~")
    (if (string-equal "д" (read-line))
	(tag-reader tag-list)
	tag-list))
  (defun links-reader (links-list)
    (format t "Введите имя и номер ссылки ~%")
    (setf links-list (list (read-line) (read)))
    (format t "Ещё ссылка? (д/н) ~")
    (if (string-equal "д" (read-line))
	(links-reader links-list)
	links-list))

    
  (let (name tags body links)
    (format t "Введите имя карточки: ~%")
    (setf name (read-line))
    (setf tags (tag-reader nil))
    (format t "Введите текст карточки: ~%")
    (setf body (read-line))
    (format t "Добавить ссылки? (д/н): ~%")
    (if (string-equal "д" (read-line))
	(setf links (links-reader nil))
	(setf links nil))
    (add-card-to-zettelkasten (list :number (length  *global-links-list*)
				    :name name
				    :tags tags
				    :body bodt
				    :links links
				    :meta nil))))
    #+end_src
