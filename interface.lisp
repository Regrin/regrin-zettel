

(defun command-handler ()
  (format t ">> ~%")
  (let ((command (read-line)))
    (cond ((string-equal command "показать")
	   (show-command-handler)
	   (command-handler))
	  ((string-equal command "добавить")
	   (add-command-handler)
	   (command-handler))
	  ((string-equal command "выйти")
	   (format t "Программа завершена ~%"))

	  (t
	   (format t "Нет такой команды ~%")
	   (command-handler)))))

(defun show-command-handler () ;Написана не безопасно, обработка ошибок основанна на том, что функция
  (format t "Введите номер карточки ~%") ;take-card-by-number возвращает nil если карточки с таким номером нет
  (let ((number (read)) ; Переписать ОБЕ!
	(card nil))
    (cond ((numberp number)
	   (setf card (take-card-by-number number))
	   (if (null card)
	       (format t "Карточка с таким номером отсутствует ~%")
	       (print-card card)))
	  (t
	   (format t "Вы ввели не номер ~%")))))
	
	


(defun print-card (card)
  (format t "~A ~A~%" (getf card :number) (getf card :name))
  (format t "~{#~A ~} ~%" (getf card :tags))
  (format t "~A~%" (getf card :body))
  (dolist (link (getf card :links))
    (format t "~A - ~A~%" (first link) (second link))))

(defun add-command-handler ()
  (defun tag-reader (tag-list)
    (format t "Введите тег: ~%")
    (setf tag-list (push (read-line) tag-list))
    (format t "Ещё тег? (д/н) ~%")
    (if (string-equal "д" (read-line))
	(tag-reader tag-list)
	tag-list))
  (defun links-reader (links-list)
    (format t "Введите имя и номер ссылки ~%")
    (setf links-list (push (list (read-line) (read)) links-list))
    (format t "Ещё ссылка? (д/н) ~%")
    (if (string-equal "д" (read-line))
	(links-reader links-list)
	links-list))

    
  (let (name tags body links)
    (format t "Введите имя карточки: ~%")
    (setf name (read-line))
    (setf tags (tag-reader nil))
    (format t "Введите текст карточки: ~%")
    (setf body (read-line))
    (format t "Добавить ссылки? (д/н): ~%")
    (if (string-equal "д" (read-line))
	(setf links (links-reader nil))
	(setf links nil))
    (add-card-to-zettelkasten (list :number (length  *global-links-list*)
				    :name name
				    :tags tags
				    :body body
				    :links links
				    :meta nil))))
    
